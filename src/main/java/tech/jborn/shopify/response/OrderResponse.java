package tech.jborn.shopify.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderResponse {
    private Long orderId;
    private List<OrderItemResponse> items;
    private BigDecimal beforeDiscountTotal;
    private BigDecimal discount;
    private BigDecimal total;
}
